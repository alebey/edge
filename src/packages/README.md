To create a package that has only a default export which is the default export from a module, add an empty file here with same name as the module. The webpack config has been designed to look for empty files here.

This saves
 - 229 bytes vs `export { default } from 'modules/<< module name >>';`
 - 165 bytes vs
   ```javascript
   import << module name >> from 'modules/<< module name >>';`

   export default << module name >>;
   ```
