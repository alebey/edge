import { TestRouter } from 'notifications-mocks';
import { mount } from 'enzyme';
import React from 'react';
import configureStore from 'redux-mock-store';

import { DISMISS_NOTIFICATION } from 'notifications';
import Notification from 'notifications/components/notification';

const mockStore = configureStore();

const getRootComponent = (initialState, props) => {
  const store = mockStore(initialState);
  const root = mount((
    <TestRouter store={store}>
      <Notification {...props} />
    </TestRouter>
  ));
  return { root, store };
};

describe('<Notification />', () => {
  it('renders properly', () => {
    const message = 'Test';
    const { root } = getRootComponent({}, { $key: 'test', message, variant: 'success' });
    const snackBarContent = root.find('SnackbarContent');
    const iconButton = snackBarContent.find('IconButton');
    const closeIcon = iconButton.find('SvgIcon');
    const messageEl = snackBarContent.find('span').at(0);
    const messageIcon = messageEl.find('SvgIcon');
    expect(snackBarContent).toHaveLength(1);
    expect(iconButton).toHaveLength(1);
    expect(closeIcon).toHaveLength(1);
    expect(messageEl.text()).toBe(message);
    expect(messageIcon).toHaveLength(1);
  });

  it('closes when the close icon is clicked', () => {
    const $key = 'test';
    const message = 'Test';
    const { root, store } = getRootComponent({}, { $key, message, variant: 'success' });
    expect(root.find('SnackbarContent').hasClass('animated')).toBe(false);
    root.find('IconButton').simulate('click');
    expect(root.update().find('SnackbarContent').hasClass('animated')).toBe(true);
    return new Promise(resolve => {
      setTimeout(() => {
        expect(store.getActions()).toEqual([ { type: DISMISS_NOTIFICATION, payload: { key: $key } } ]);
        resolve();
      }, 1000);
    });
  });

  it('calls the onDismiss callback with true when the notifications is closed by the user', () => {
    const $key = 'test';
    const message = 'Test';
    const onDismiss = jest.fn();
    const { root } = getRootComponent({}, { $key, message, onDismiss, variant: 'success' });
    root.find('IconButton').simulate('click');
    expect(onDismiss).toHaveBeenCalledWith(true);
  });

  it('calls the onDismiss callback with false when the notifications is auto-dismissed', () => {
    const $key = 'test';
    const message = 'Test';
    const onDismiss = jest.fn();
    getRootComponent({}, { $key, autoDismissMillis: 1, message, onDismiss, variant: 'success' });
    return new Promise(resolve => {
      setTimeout(() => {
        expect(onDismiss).toHaveBeenCalledWith(false);
        resolve();
      }, 1000);
    });
  });

  it('will only dismiss the notification once', () => {
    const $key = 'test';
    const message = 'Test';
    const { root, store } = getRootComponent({}, { $key, message, variant: 'success' });
    root.find('IconButton').simulate('click');
    root.find('IconButton').simulate('click');
    return new Promise(resolve => {
      setTimeout(() => {
        expect(store.getActions()).toEqual([ { type: DISMISS_NOTIFICATION, payload: { key: $key } } ]);
        resolve();
      }, 1000);
    });
  });

  it('will not auto-dismiss the notification when autoDismissMillis is 0', () => {
    const $key = 'test';
    const message = 'Test';
    const { store } = getRootComponent({}, { $key, autoDismissMillis: 0, message, variant: 'success' });
    return new Promise(resolve => {
      setTimeout(() => {
        expect(store.getActions()).toEqual([]);
        resolve();
      }, 1100);
    });
  });

  it('will auto-dismiss the notification in 1 second when autoDismissMillis is 1', () => {
    const $key = 'test';
    const message = 'Test';
    const { store } = getRootComponent({}, { $key, autoDismissMillis: 1, message, variant: 'success' });
    return new Promise(resolve => {
      setTimeout(() => {
        expect(store.getActions()).toEqual([ { type: DISMISS_NOTIFICATION, payload: { key: $key } } ]);
        resolve();
      }, 1100);
    });
  });
});
