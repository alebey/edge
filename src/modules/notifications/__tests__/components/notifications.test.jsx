import { List } from 'immutable';
import { TestRouter } from 'notifications-mocks';
import { mount } from 'enzyme';
import React from 'react';
import configureStore from 'redux-mock-store';

import { Notifications } from 'notifications';

const mockStore = configureStore();

const getRootComponent = (initialState, props) => {
  const store = mockStore(initialState);
  const root = mount((
    <TestRouter store={store}>
      <Notifications {...props} />
    </TestRouter>
  ));
  return { root, store };
};

describe('<Notifications />', () => {
  it('renders properly with 0 notifications', () => {
    const notifications = List();
    const { root } = getRootComponent({ notifications });
    const snackbar = root.find('Snackbar');
    expect(snackbar).toHaveLength(1);
    expect(snackbar.prop('anchorOrigin')).toEqual({ horizontal: 'right', vertical: 'top' });
    expect(snackbar.prop('open')).toBe(false);
  });

  it('renders properly with 1 notification', () => {
    const notifications = List([ { key: 'test', message: 'Test One', variant: 'success' } ]);
    const { root } = getRootComponent({ notifications });
    const snackbar = root.find('Snackbar');
    const notificationEls = snackbar.find('Notification');
    expect(snackbar).toHaveLength(1);
    expect(snackbar.prop('open')).toBe(true);
    expect(notificationEls).toHaveLength(1);
  });

  it('renders properly with 2 notifications', () => {
    const notifications = List([
      { key: 'test1', message: 'Test One', variant: 'success' },
      { key: 'test2', message: 'Test Two', variant: 'error' }
    ]);
    const { root } = getRootComponent({ notifications });
    const snackbar = root.find('Snackbar');
    const notificationEls = snackbar.find('Notification');
    expect(snackbar).toHaveLength(1);
    expect(snackbar.prop('open')).toBe(true);
    expect(notificationEls).toHaveLength(2);
  });

  it("renders properly with position 'top left'", () => {
    const notifications = List([ { key: 'test', message: 'Test One', variant: 'success' } ]);
    const { root } = getRootComponent({ notifications }, { position: 'top left' });
    const snackbar = root.find('Snackbar');
    expect(snackbar.prop('anchorOrigin')).toEqual({ horizontal: 'left', vertical: 'top' });
  });

  it("renders properly with position 'top center'", () => {
    const notifications = List([ { key: 'test', message: 'Test One', variant: 'success' } ]);
    const { root } = getRootComponent({ notifications }, { position: 'top center' });
    const snackbar = root.find('Snackbar');
    expect(snackbar.prop('anchorOrigin')).toEqual({ horizontal: 'center', vertical: 'top' });
  });

  it("renders properly with position 'bottom right'", () => {
    const notifications = List([ { key: 'test', message: 'Test One', variant: 'success' } ]);
    const { root } = getRootComponent({ notifications }, { position: 'bottom right' });
    const snackbar = root.find('Snackbar');
    expect(snackbar.prop('anchorOrigin')).toEqual({ horizontal: 'right', vertical: 'bottom' });
  });

  it("renders in the default 'top right' location with invalid position", () => {
    const notifications = List([ { key: 'test', message: 'Test One', variant: 'success' } ]);
    const { root } = getRootComponent({ notifications }, { position: 'blah blah' });
    const snackbar = root.find('Snackbar');
    expect(snackbar.prop('anchorOrigin')).toEqual({ horizontal: 'right', vertical: 'top' });
  });
});
