import drain from 'modules/drain';

import { notificationActions } from 'notifications';
import { configureStore } from 'notifications-mocks';
import { notificationMessages } from 'notifications-mocks/constants';

describe('redux notifications', () => {
  it('properly updates the store when notifications are added', drain(function* () {
    const store = configureStore();
    const errorMessage = notificationMessages.error;
    const infoMessage = notificationMessages.info;
    const successMessage = notificationMessages.success;
    const warningMessage = notificationMessages.warning;
    yield store.dispatch(notificationActions.notifyError(errorMessage));
    yield store.dispatch(notificationActions.notifyInfo(infoMessage));
    yield store.dispatch(notificationActions.notifySuccess(successMessage));
    yield store.dispatch(notificationActions.notifyWarning(warningMessage));
    const errorEntry = { key: '$$notification-0$$', message: errorMessage, variant: 'error' };
    const infoEntry = { key: '$$notification-1$$', message: infoMessage, variant: 'info' };
    const successEntry = { key: '$$notification-2$$', message: successMessage, variant: 'success' };
    const warningEntry = { key: '$$notification-3$$', message: warningMessage, variant: 'warning' };
    expect(store.getState().notifications.toJSON()).toEqual([ errorEntry, infoEntry, successEntry, warningEntry ]);
  }));

  it('properly updates the store when notifications with options are added', drain(function* () {
    const store = configureStore();
    const errorMessage = notificationMessages.error;
    const infoMessage = notificationMessages.info;
    const successMessage = notificationMessages.success;
    const warningMessage = notificationMessages.warning;
    const opts = { autoDismissMillis: 1, onDismiss: () => {} };
    yield store.dispatch(notificationActions.notifyError(errorMessage, { key: 'errorKey', ...opts }));
    yield store.dispatch(notificationActions.notifyInfo(infoMessage, { key: 'infoKey', ...opts }));
    yield store.dispatch(notificationActions.notifySuccess(successMessage, { key: 'successKey', ...opts }));
    yield store.dispatch(notificationActions.notifyWarning(warningMessage, { key: 'warningKey', ...opts }));
    const errorEntry = { key: 'errorKey', message: errorMessage, ...opts, variant: 'error' };
    const infoEntry = { key: 'infoKey', message: infoMessage, ...opts, variant: 'info' };
    const successEntry = { key: 'successKey', message: successMessage, ...opts, variant: 'success' };
    const warningEntry = { key: 'warningKey', message: warningMessage, ...opts, variant: 'warning' };
    expect(store.getState().notifications.toJSON()).toEqual([ errorEntry, infoEntry, successEntry, warningEntry ]);
  }));

  it('will not add a notification if another notification with the same key already exists', drain(function* () {
    const store = configureStore();
    const key = 'sharedKey';
    const message = 'First Message';
    const message2 = 'Second Message';
    yield store.dispatch(notificationActions.notifyInfo(message, { key }));
    yield store.dispatch(notificationActions.notifyInfo(message2, { key }));
    expect(store.getState().notifications.toJSON()).toEqual([ { key, message, variant: 'info' } ]);
  }));

  it('properly updates the store when notifications are dismissed', drain(function* () {
    const store = configureStore();
    const errorMessage = notificationMessages.error;
    const infoMessage = notificationMessages.info;
    const successMessage = notificationMessages.success;
    const warningMessage = notificationMessages.warning;
    yield store.dispatch(notificationActions.notifyError(errorMessage, { key: 'errorKey' }));
    yield store.dispatch(notificationActions.notifyInfo(infoMessage, { key: 'infoKey' }));
    yield store.dispatch(notificationActions.notifySuccess(successMessage, { key: 'successKey' }));
    yield store.dispatch(notificationActions.notifyWarning(warningMessage, { key: 'warningKey' }));
    const infoEntry = { key: 'infoKey', message: infoMessage, variant: 'info' };
    const warningEntry = { key: 'warningKey', message: warningMessage, variant: 'warning' };
    yield store.dispatch(notificationActions.dismissNotification('errorKey'));
    yield store.dispatch(notificationActions.dismissNotification('successKey'));
    expect(store.getState().notifications.toJSON()).toEqual([ infoEntry, warningEntry ]);
  }));
});
