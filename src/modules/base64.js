/**
* These functions encode and decode to utf-8 and url-safe base64.
* refs: https://developer.mozilla.org/en-US/docs/Web/API/WindowBase64/Base64_encoding_and_decoding
*       https://docs.python.org/2/library/base64.html#base64.urlsafe_b64encode
*
* Python equivalents to these functions:
*   - decodeBase64: `base64.urlsafe_b64decode('<< string >>').decode('utf-8')`
*   - encodeBase64: `base64.urlsafe_b64encode('<< string >>'.encode('utf-8'))`
*/

const decode = str => decodeURIComponent(escape(atob(str.replace(/_/g, '/').replace(/-/g, '+'))));

const encodeNonWebSafe = str => btoa(unescape(encodeURIComponent(str)));

const encode = str => encodeNonWebSafe(str).replace(/\//g, '_').replace(/\+/g, '-');

export default { decode, encode, encodeNonWebSafe };
