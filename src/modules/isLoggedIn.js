import getJwt from './getJwt';

export default keyName => {
  try {
    return getJwt(keyName).exp * 1000 > new Date().getTime();
  } catch (e) {
    return false;
  }
};
