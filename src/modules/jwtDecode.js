import base64 from './base64';

export default token => {
  const [ , payload ] = token.split('.').reverse();
  try {
    return JSON.parse(base64.decode(payload));
  } catch (e) {
    throw new TypeError(`Invalid JWT token: "${token}"`);
  }
};
