export default class LocalStoregeMock {
  constructor() {
    this.windowLocalStorage = window.localStorage;
    this.clear = jest.fn(this.jestClear);
    this.getItem = jest.fn(this.jestGetItem);
    this.setItem = jest.fn(this.jestSetItem);
    window.localStorage = this;
  }

  deactivate() {
    window.localStorage = this.windowLocalStorage;
    window.localStorage.clear();
  }

  items = {};

  jestClear() {
    this.items = {};
  }

  jestGetItem = name => this.items[name];

  jestSetItem(name, value) {
    this.items[name] = value;
  }
}
