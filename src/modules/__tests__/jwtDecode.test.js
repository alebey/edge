import jwtDecode from 'modules/jwtDecode';

describe('jwtDecode', () => {
  it('decodes properly', () => {
    const jwtToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiRWRnZSwgR25hciBFZGdlIn0.' +
                     'rkiN0u6pMNSsAGpOrd-Wb7jvQOW9DRYO6k71XtwniqI';
    expect(jwtDecode(jwtToken)).toEqual({ name: 'Edge, Gnar Edge' });
  });

  it('throws an error when the token cannot be decoded', () => {
    try {
      jwtDecode('not a token');
    } catch ({ message }) {
      expect(message).toBe('Invalid JWT token: "not a token"');
    }
  });
});
