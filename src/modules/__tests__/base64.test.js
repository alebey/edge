import base64 from 'modules/base64';

const expectations = [
  { decoded: '  >', encoded: 'ICA-' },
  { decoded: '  ?', encoded: 'ICA_' },
  { decoded: '  ~', encoded: 'ICB-' },
  { decoded: '     >', encoded: 'ICAgICA-' },
  { decoded: '     ?', encoded: 'ICAgICA_' },
  { decoded: '     ~', encoded: 'ICAgICB-' },
  { decoded: '        >', encoded: 'ICAgICAgICA-' },
  { decoded: '        ?', encoded: 'ICAgICAgICA_' },
  { decoded: '        ~', encoded: 'ICAgICAgICB-' },
  { decoded: '           >', encoded: 'ICAgICAgICAgICA-' },
  { decoded: '✓ à la mode', encoded: '4pyTIMOgIGxhIG1vZGU=' },
  { decoded: '\n', encoded: 'Cg==' },
  { decoded: 'Gnar Powder', encoded: 'R25hciBQb3dkZXI=' },
  {
    decoded:
      '᚛᚛ᚉᚑᚅᚔᚉᚉᚔᚋ ᚔᚈᚔ ᚍᚂᚐᚅᚑ ᚅᚔᚋᚌᚓᚅᚐ᚜',
    encoded:
      '4Zqb4Zqb4ZqJ4ZqR4ZqF4ZqU4ZqJ4ZqJ4ZqU4ZqL4ZqA4ZqU4ZqI4ZqU4Z' +
      'qA4ZqN4ZqC4ZqQ4ZqF4ZqR4ZqA4ZqF4ZqU4ZqL4ZqM4ZqT4ZqF4ZqQ4Zqc'
  },
  {
    decoded:
      'ᚠᛇᚻ᛫ᛒᛦᚦ᛫ᚠᚱᚩᚠᚢᚱ᛫ᚠᛁᚱᚪ᛫ᚷᛖᚻᚹᛦᛚᚳᚢᛗᛋᚳᛖᚪᛚ᛫ᚦᛖᚪᚻ᛫ᛗᚪᚾᚾᚪ᛫ᚷᛖᚻᚹᛦᛚᚳ᛫ᛗᛁᚳᛚᚢᚾ᛫ᚻᛦᛏ᛫ᛞᚫᛚᚪᚾᚷᛁᚠ᛫ᚻᛖ᛫ᚹᛁᛚᛖ᛫ᚠᚩᚱ᛫ᛞᚱᛁᚻᛏᚾᛖ᛫ᛞᚩᛗᛖᛋ᛫ᚻᛚᛇᛏᚪᚾ᛬',
    encoded:
      '4Zqg4ZuH4Zq74Zur4ZuS4Zum4Zqm4Zur4Zqg4Zqx4Zqp4Zqg4Zqi4Zqx4Zur4Zqg4ZuB4Zqx4Zqq4Zur4Zq3' +
      '4ZuW4Zq74Zq54Zum4Zua4Zqz4Zqi4ZuX4ZuL4Zqz4ZuW4Zqq4Zua4Zur4Zqm4ZuW4Zqq4Zq74Zur4ZuX4Zqq' +
      '4Zq-4Zq-4Zqq4Zur4Zq34ZuW4Zq74Zq54Zum4Zua4Zqz4Zur4ZuX4ZuB4Zqz4Zua4Zqi4Zq-4Zur4Zq74Zum' +
      '4ZuP4Zur4Zue4Zqr4Zua4Zqq4Zq-4Zq34ZuB4Zqg4Zur4Zq74ZuW4Zur4Zq54ZuB4Zua4ZuW4Zur4Zqg4Zqp' +
      '4Zqx4Zur4Zue4Zqx4ZuB4Zq74ZuP4Zq-4ZuW4Zur4Zue4Zqp4ZuX4ZuW4ZuL4Zur4Zq74Zua4ZuH4ZuP4Zqq' +
      '4Zq-4Zus'
  },
  {
    decoded:
      "If you're walking down the right path and you're willing to keep walking, eventually you'll make progress.",
    encoded:
      'SWYgeW91J3JlIHdhbGtpbmcgZG93biB0aGUgcmlnaHQgcGF0aCBhbmQgeW91J3JlIHdpbGxp' +
      'bmcgdG8ga2VlcCB3YWxraW5nLCBldmVudHVhbGx5IHlvdSdsbCBtYWtlIHByb2dyZXNzLg=='
  }
];

describe('base64', () => {
  it('encodes properly', () => {
    expectations.forEach(({ decoded, encoded }) => {
      expect(base64.encode(decoded)).toBe(encoded);
    });
  });

  it('decodes properly', () => {
    expectations.forEach(({ decoded, encoded }) => {
      expect(base64.decode(encoded)).toBe(decoded);
    });
  });
});
