import Chance from 'chance';

import handleChange from 'modules/handleChange';

const chance = new Chance();

describe('handleChange', () => {
  it('properly updates state', () => {
    const cb = () => {};
    const fn = jest.fn();
    const context = { setState: fn };
    const boundHandleChange = context::handleChange;
    const fieldName = chance.word();
    const checked = chance.bool();
    const value = chance.sentence();
    const simpleValue = chance.sentence();
    boundHandleChange(fieldName)({ target: { checked } });
    expect(fn).toHaveBeenCalledWith({ [fieldName]: checked });
    boundHandleChange(fieldName)({ target: { value } });
    expect(fn).toHaveBeenCalledWith({ [fieldName]: value });
    boundHandleChange(fieldName)(simpleValue);
    expect(fn).toHaveBeenCalledWith({ [fieldName]: simpleValue });
    boundHandleChange(fieldName, cb)({ target: { checked } });
    expect(fn).toHaveBeenCalledWith({ [fieldName]: checked }, cb);
    boundHandleChange(fieldName, cb)({ target: { value } });
    expect(fn).toHaveBeenCalledWith({ [fieldName]: value }, cb);
    boundHandleChange(fieldName, cb)(simpleValue);
    expect(fn).toHaveBeenCalledWith({ [fieldName]: simpleValue }, cb);
  });

  it("calls 'beforeSetState' when provided", () => {
    const cb = () => {};
    const fn = jest.fn();
    const beforeSetState = jest.fn();
    const context = { setState: fn };
    const boundHandleChange = context::handleChange;
    const fieldName = chance.word();
    const value = chance.sentence();
    boundHandleChange(fieldName, null, { beforeSetState })({ target: { value } });
    expect(beforeSetState).toHaveBeenCalled();
    expect(fn).toHaveBeenCalledWith({ [fieldName]: value });
    fn.mockClear();
    beforeSetState.mockClear();
    expect(fn).not.toHaveBeenCalled();
    expect(beforeSetState).not.toHaveBeenCalled();
    boundHandleChange(fieldName, undefined, { beforeSetState })({ target: { value } });
    expect(beforeSetState).toHaveBeenCalled();
    expect(fn).toHaveBeenCalledWith({ [fieldName]: value });
    fn.mockClear();
    beforeSetState.mockClear();
    boundHandleChange(fieldName, cb, { beforeSetState })({ target: { value } });
    expect(beforeSetState).toHaveBeenCalled();
    expect(fn).toHaveBeenCalledWith({ [fieldName]: value }, cb);
  });
});
