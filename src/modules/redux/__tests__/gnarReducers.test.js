import { createStore } from 'redux';
import { handleActions } from 'redux-actions';

import gnarReducers from '../gnarReducers';

describe('gnarReducers', () => {
  it('properly handles a single store node with only basic reducers defined as a string', () => {
    const DO_IT = 'DO_IT';
    const payload = { you: 'got this' };
    const store = createStore(
      gnarReducers({
        testStoreNode: DO_IT
      })
    );
    store.dispatch({ type: DO_IT, payload });
    expect(store.getState().testStoreNode).toEqual(payload);
  });

  it('properly handles a single store node with only basic reducers defined as an array', () => {
    const DO_IT = 'DO_IT';
    const payload = { you: 'got this' };
    const store = createStore(
      gnarReducers({
        testStoreNode: [ DO_IT ]
      })
    );
    store.dispatch({ type: DO_IT, payload });
    expect(store.getState().testStoreNode).toEqual(payload);
  });

  it('properly handles a single store node with only basic reducers', () => {
    const DO_IT = 'DO_IT';
    const payload = { you: 'got this' };
    const store = createStore(
      gnarReducers({
        testStoreNode: {
          basicReducers: [ DO_IT ]
        }
      })
    );
    store.dispatch({ type: DO_IT, payload });
    expect(store.getState().testStoreNode).toEqual(payload);
  });

  it('properly handles a single store node with only basic reducers and an initialState', () => {
    const DO_IT = 'DO_IT';
    const initialState = { me: 'tarzan', you: 'go girl' };
    const payload = { you: 'got this' };
    const store = createStore(
      gnarReducers({
        testStoreNode: {
          initialState,
          basicReducers: [ DO_IT ]
        }
      })
    );
    store.dispatch({ type: DO_IT, payload });
    expect(store.getState().testStoreNode).toEqual({ ...initialState, ...payload });
  });

  it('properly handles a single store node with only custom reducers', () => {
    const DO_IT_CUSTOM = 'DO_IT_CUSTOM';
    const store = createStore(
      gnarReducers({
        testStoreNode: {
          customReducers: {
            [DO_IT_CUSTOM]: (state, { payload }) => ({ ...state, you: `${payload.you}, dude` })
          }
        }
      })
    );
    store.dispatch({ type: DO_IT_CUSTOM, payload: { you: 'got this' } });
    expect(store.getState().testStoreNode).toEqual({ you: 'got this, dude' });
  });

  it('properly handles a single store node with only custom reducers and an initialState', () => {
    const DO_IT_CUSTOM = 'DO_IT_CUSTOM';
    const initialState = { me: 'tarzan', you: 'go girl' };
    const store = createStore(
      gnarReducers({
        testStoreNode: {
          initialState,
          customReducers: {
            [DO_IT_CUSTOM]: (state, { payload }) => ({ ...state, you: `${state.you}... you ${payload.you}` })
          }
        }
      })
    );
    store.dispatch({ type: DO_IT_CUSTOM, payload: { you: 'got this' } });
    expect(store.getState().testStoreNode).toEqual({ me: 'tarzan', you: 'go girl... you got this' });
  });

  it('properly handles a single store node with basic and custom reducers', () => {
    const DO_IT = 'DO_IT';
    const DO_IT_CUSTOM = 'DO_IT_CUSTOM';
    const store = createStore(
      gnarReducers({
        testStoreNode: {
          basicReducers: [ DO_IT ],
          customReducers: {
            [DO_IT_CUSTOM]: (state, { payload }) => ({ ...state, you: `${payload.you}, dude` })
          }
        }
      })
    );
    store.dispatch({ type: DO_IT, payload: { me: 'tarzan' } });
    store.dispatch({ type: DO_IT_CUSTOM, payload: { you: 'got this' } });
    expect(store.getState().testStoreNode).toEqual({ me: 'tarzan', you: 'got this, dude' });
  });

  it('ignores pre-defined reducers in the store', () => {
    const DO_IT = 'DO_IT';
    const store = createStore(
      gnarReducers({
        testStoreNode: handleActions({
          [DO_IT]: (state, { payload }) => ({ ...state, ...payload })
        }, { you: 'jane' })
      })
    );
    store.dispatch({ type: DO_IT, payload: { me: 'tarzan' } });
    expect(store.getState().testStoreNode).toEqual({ me: 'tarzan', you: 'jane' });
  });

  it('properly handles a single store branch with two nodes', () => {
    const DO_IT = 'DO_IT';
    const payload = { you: 'got this' };
    const store = createStore(
      gnarReducers({
        testStoreNode: {
          testSubNode: {
            basicReducers: [ DO_IT ]
          }
        }
      })
    );
    store.dispatch({ type: DO_IT, payload });
    expect(store.getState().testStoreNode.testSubNode).toEqual(payload);
  });

  it('throws the correct error when basicReducers is not an array', () => {
    expect(() => gnarReducers({
      testStoreNode: {
        basicReducers: null
      }
    })).toThrow(new TypeError('Expected array or string at testStoreNode.basicReducers, received null'));
  });

  it('throws the correct error when a nested basicReducers is not an array', () => {
    expect(() => gnarReducers({
      testStoreNode: {
        testSubNode: {
          basicReducers: null
        }
      }
    })).toThrow(new TypeError('Expected array or string at testStoreNode.testSubNode.basicReducers, received null'));
  });

  it('throws the correct error when customReducers is not an object', () => {
    expect(() => gnarReducers({
      testStoreNode: {
        customReducers: null
      }
    })).toThrow(new TypeError('Expected object at testStoreNode.customReducers, received null'));
  });

  it('throws the correct error when a nested customReducers is not an object', () => {
    expect(() => gnarReducers({
      testStoreNode: {
        testSubNode: {
          customReducers: null
        }
      }
    })).toThrow(new TypeError('Expected object at testStoreNode.testSubNode.customReducers, received null'));
  });

  it('throws the correct error when an item in a basicReducers array is not a string', () => {
    expect(() => gnarReducers({
      testStoreNode: {
        basicReducers: [ 'SOME_ACTION', 42 ]
      }
    })).toThrow(new TypeError('Expected string at testStoreNode.basicReducers.1, received 42'));
  });

  it('throws the correct error when a nested item in a basicReducers array is not a string', () => {
    expect(() => gnarReducers({
      testStoreNode: {
        testSubNode: {
          basicReducers: [ 'SOME_ACTION', 42 ]
        }
      }
    })).toThrow(new TypeError('Expected string at testStoreNode.testSubNode.basicReducers.1, received 42'));
  });

  it('throws the correct error when an item in a customReducers array is not a function', () => {
    expect(() => gnarReducers({
      testStoreNode: {
        customReducers: {
          SOME_ACTION: () => 'Mock reducer',
          SOME_OTHER_ACTION: 'Oops'
        }
      }
    })).toThrow(new TypeError('Expected function at testStoreNode.customReducers.SOME_OTHER_ACTION, received "Oops"'));
  });

  it('throws the correct error when an item in a customReducers array is not a function', () => {
    const msg = 'Expected function at testStoreNode.testSubNode.customReducers.SOME_OTHER_ACTION, received "Oops"';
    expect(() => gnarReducers({
      testStoreNode: {
        testSubNode: {
          customReducers: {
            SOME_ACTION: () => 'Mock reducer',
            SOME_OTHER_ACTION: 'Oops'
          }
        }
      }
    })).toThrow(new TypeError(msg));
  });

  it('properly handles a Map as an defaultInitialState', () => {
    class Map {
      asMutable = ''

      merge(payload) {
        this.internalState = payload;
        return this;
      }
    }
    const DO_IT = 'DO_IT';
    const payload = { you: 'got this' };
    const testState = new Map();
    testState.internalState = payload;
    const store = createStore(
      gnarReducers({
        testStoreNode: {
          basicReducers: [ DO_IT ]
        }
      }, () => new Map())
    );
    store.dispatch({ type: DO_IT, payload });
    expect(store.getState().testStoreNode).toEqual(testState);
  });

  it('properly handles a defaultReducer', () => {
    const DO_IT = 'DO_IT';
    const store = createStore(
      gnarReducers({
        testStoreNode: {
          basicReducers: [ DO_IT ]
        }
      }, null, (state, { payload }) => ({ ...state, you: `${payload.you}... and, we're done` }))
    );
    store.dispatch({ type: DO_IT, payload: { you: 'got this' } });
    expect(store.getState().testStoreNode).toEqual({ you: "got this... and, we're done" });
  });
});
